package cryptarithme;

import java.util.ArrayList;
import java.util.List;

/**
 * Domaine de définition que chaque variable peut prendre. Ici de 0 à 9
 *
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class Domain implements Cloneable {
	
	private List<Integer> valeurs;
	private int min;
	private int max;
	
	public Domain(List<Integer> valeurs) {
		this.valeurs = valeurs;
		this.min = valeurs.get(0);
		this.max = valeurs.get(valeurs.size()-1);
	}
	
	public Domain(Integer min, Integer max, Integer step) {
		this.valeurs = new ArrayList<>();
		this.min = min;
		this.max = max;
		for(int i=min; i<=max; i+=step) {
			this.valeurs.add(i);
		}
	}
	
	@Override
	public Domain clone() throws CloneNotSupportedException {
		return new Domain(new ArrayList<>(this.valeurs));
	}

	/**
	 * @return the valeurs
	 */
	public List<Integer> getValeurs() {
		return valeurs;
	}

	/**
	 * @param valeurs the valeurs to set
	 */
	public void setValeurs(List<Integer> valeurs) {
		this.valeurs = valeurs;
	}

	/**
	 * @return the min
	 */
	public int getMin() {
		return min;
	}

	/**
	 * @param min the min to set
	 */
	public void setMin(int min) {
		this.min = min;
	}

	/**
	 * @return the max
	 */
	public int getMax() {
		return max;
	}

	/**
	 * @param max the max to set
	 */
	public void setMax(int max) {
		this.max = max;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + max;
		result = prime * result + min;
		result = prime * result + ((valeurs == null) ? 0 : valeurs.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Domain other = (Domain) obj;
		if (max != other.max)
			return false;
		if (min != other.min)
			return false;
		if (valeurs == null) {
			if (other.valeurs != null)
				return false;
		} else if (!valeurs.equals(other.valeurs))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.valeurs.toString();
	}	
}
