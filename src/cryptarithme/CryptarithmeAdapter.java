package cryptarithme;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import ia.solve.Solveable;

/**
 * Adapter qui permet de résoudre ce problème avec le système de résolution en IA
 *
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class CryptarithmeAdapter implements Solveable {

	/**
	 * Le problème à résoudre
	 */
	private Cryptarithme cryptarithme;
	
	/**
	 * Les actions à effectuer sur les variable. Ici modifier les valeurs de chaque variable de façon a ce que chaque variable
	 * respecte les conditions (equation et alldifferent)
	 * Clé = numéro de l'action généré dans la méthode getActions()
	 * Valeur = map clé -> variable ; valeur -> la valeur à affecter à la variable
	 */
	private Map<Integer, Map<Variable, Integer>> actionsVariables;
	
	public CryptarithmeAdapter(Cryptarithme cryptarithme) {
		this.cryptarithme = cryptarithme;
		this.actionsVariables = new HashMap<>();
	}
	
	public boolean soluceFind() {
		return this.cryptarithme.soluceFind();
	}
	
	@Override
	public boolean checkActions() {
		return this.actionsVariables != null && !this.actionsVariables.isEmpty();
	}

	@Override
	public boolean checkActions(int action) {
		return this.actionsVariables.containsKey(action);
	}

	@Override
	public String applyActions(int action) {
		Map<Variable, Integer> variableValue = this.actionsVariables.get(action);
		for(Entry<Variable, Integer> entry : variableValue.entrySet()) {
			Variable variable = entry.getKey();
			variable.setValeur(entry.getValue());
			
			//On supprime toutes les actions afin d'éviter un java.lang.OutOfMemoryError
			this.actionsVariables = new HashMap<>();
			
			return variable.toString();
		}
		return "";
	}

	
	/**
	 * On génére tous les états possibles de chaque variable selon leur domaine de définition,
	 * exclus la valeur courante de la variable et les valeurs déjà prises par d'autres variables
	 * @return
	 */
	@Override
	public List<Integer> getActions() {
		this.actionsVariables = new HashMap<>();
		int action = 0;
		
		for(Variable variable : this.cryptarithme.getVariables()) {
			for(Integer valeur : variable.getDomain().getValeurs()) {
				//Si la valeur n'est pas déjà affecté à une autre variable
				if(!this.isAlreadyAffect(valeur)) {
					this.actionsVariables.put(action, Collections.singletonMap(variable, valeur));
					action++;	
				}
			}
		}
		
		return new ArrayList<>(this.actionsVariables.keySet());
	}

	/**
	 * Vérifie si une valeur est déjà affecté à une variable
	 * @param valeur à vérifier
	 * @return vrai si déjà affectée. Faux dans le cas contraire
	 */
	private boolean isAlreadyAffect(Integer valeur) {
		for(Variable variable : this.cryptarithme.getVariables()) {
			if(variable.getValeur().equals(valeur))
				return true;
		}
		return false;
	}
	
	@Override
	public Solveable clone() throws CloneNotSupportedException {
		CryptarithmeAdapter cryptarithmeAdapter = new CryptarithmeAdapter(this.cryptarithme.clone());
		cryptarithmeAdapter.getActions();
		return cryptarithmeAdapter;
	}

	@Override
	public void clear() {
		this.actionsVariables = new HashMap<>();
	}
	
	/**
	 * @return the cryptarithme
	 */
	public Cryptarithme getCryptarithme() {
		return cryptarithme;
	}

	/**
	 * @param cryptarithme the cryptarithme to set
	 */
	public void setCryptarithme(Cryptarithme cryptarithme) {
		this.cryptarithme = cryptarithme;
	}

	/**
	 * @return the actionsVariables
	 */
	public Map<Integer, Map<Variable, Integer>> getActionsVariables() {
		return actionsVariables;
	}

	/**
	 * @param actionsVariables the actionsVariables to set
	 */
	public void setActionsVariables(Map<Integer, Map<Variable, Integer>> actionsVariables) {
		this.actionsVariables = actionsVariables;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cryptarithme == null) ? 0 : cryptarithme.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CryptarithmeAdapter other = (CryptarithmeAdapter) obj;
		if (cryptarithme == null) {
			if (other.cryptarithme != null)
				return false;
		} else if (!cryptarithme.equals(other.cryptarithme))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.cryptarithme.toString();
	}
	
	public String toStringWord() {
		return this.cryptarithme.toStringWord();
	}
}
