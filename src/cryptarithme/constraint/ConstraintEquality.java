package cryptarithme.constraint;

import java.util.ArrayList;
import java.util.List;
import cryptarithme.Variable;
import cryptarithme.Word;

/**
 * Contrainte qui permet de s'assurer que toutes les variables respectent l'équation avec d'autres variables qui sont les conditions.
 * En l'occurence une somme
 * 
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class ConstraintEquality implements Constraint {

	private List<Word> variables;
	private List<Word> conditions;
	
	public ConstraintEquality(List<Word> variables, List<Word> conditions) {
		this.variables = variables;
		this.conditions = conditions;
	}

	/**
	 * Exemple avec SEND + MORE = MONEY
	 * S ∗ 1000 + E ∗ 100 + N ∗ 10 + D + M ∗ 1000 + O ∗ 100 + R ∗ 10 + E
	 * =
	 * M ∗ 10000 + O ∗ 1000 + N ∗ 100 + E ∗ 10 + Y
	 */
	@Override
	public boolean check() {
		return this.resultVariables() == this.resultConditions();
	}

	@Override
	public double calculHHeuristic() {
		return Math.abs(this.resultVariables() - this.resultConditions());
	}
	
	public int resultConditions() {
		return this.result(this.conditions);
	}
	
	public int resultVariables() {
		return this.result(this.variables);
	}
	
	/**
	 * Calcule la somme des mots selon les valeurs des variables
	 * @param words
	 * @return la somme de tous les mots
	 */
	private int result(List<Word> words) {
		int result = 0;
		
		for(Word word : words) {
			result += word.sum();
		}
		
		return result;
	}

	@Override
	public Constraint clone() throws CloneNotSupportedException {
		return new ConstraintEquality(this.variables, this.conditions);
	}
	
	@Override
	public void setVariablesCloned(List<Variable> variablesCloned) {
		List<Word> variables = new ArrayList<>();
		List<Word> conditions = new ArrayList<>();
		
		for(Word word : this.variables) {
			Word wordCloned = new Word(new ArrayList<>());
			for(Variable variable : word.getVariables()) {
				for(Variable variableCloned : variablesCloned) {
					if(variable.equals(variableCloned)) {
						wordCloned.getVariables().add(variableCloned);
					}
				}
			}
			variables.add(wordCloned);
		}
		
		for(Word word : this.conditions) {
			Word wordCloned = new Word(new ArrayList<>());
			for(Variable variable : word.getVariables()) {
				for(Variable variableCloned : variablesCloned) {
					if(variable.equals(variableCloned)) {
						wordCloned.getVariables().add(variableCloned);
					}
				}
			}
			conditions.add(wordCloned);
		}
		
		this.variables = variables;
		this.conditions = conditions;
	}
	
	/**
	 * @return the variables
	 */
	public List<Word> getVariables() {
		return variables;
	}
	
	/**
	 * @param variables the variables to set
	 */
	public void setVariables(List<Word> variables) {
		this.variables = variables;
	}

	/**
	 * @return the conditions
	 */
	public List<Word> getConditions() {
		return conditions;
	}

	/**
	 * @param conditions the conditions to set
	 */
	public void setConditions(List<Word> conditions) {
		this.conditions = conditions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((conditions == null) ? 0 : conditions.hashCode());
		result = prime * result + ((variables == null) ? 0 : variables.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConstraintEquality other = (ConstraintEquality) obj;
		if (conditions == null) {
			if (other.conditions != null)
				return false;
		} else if (!conditions.equals(other.conditions))
			return false;
		if (variables == null) {
			if (other.variables != null)
				return false;
		} else if (!variables.equals(other.variables))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for(Word word : this.variables) {
			sb.append(word.toString());
			sb.append(" + ");
		}
		
		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);
		
		sb.append(" = ");
		
		for(Word word : this.conditions) {
			sb.append(word.toString());
			sb.append(" + ");
		}
		
		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);
		
		return sb.toString();
	}
}
