package cryptarithme.constraint;

import java.util.ArrayList;
import java.util.List;
import cryptarithme.Variable;

/**
 * Contrainte qui permet de s'assurer que toutes les variables ont des valeurs uniques
 * 
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class ConstraintAllDiff implements Constraint {

	private List<Variable> variables;
	
	public ConstraintAllDiff(List<Variable> variables) {
		this.variables = variables;
	}

	@Override
	public boolean check() {
		return countVariablesUniques() == this.variables.size();
	}
	
	@Override
	public double calculHHeuristic() {
		return Math.abs(this.countVariablesUniques() - this.variables.size());
	}
	
	/**
	 * Compter le nombre de variables qui ont des valeurs uniques
	 * @return le nombre de variables uniques
	 */
	private int countVariablesUniques() {
		List<Integer> variablesUniques = new ArrayList<>();
		
		for(Variable variable : this.variables) {
			if(!variablesUniques.contains(variable.getValeur())) {
				variablesUniques.add(variable.getValeur());
			}
		}
		
		return variablesUniques.size();
	}
	
	@Override
	public Constraint clone() throws CloneNotSupportedException {
		return new ConstraintAllDiff(this.variables);
	}

	@Override
	public void setVariablesCloned(List<Variable> variablesCloned) {
		this.setVariables(variablesCloned);
	}
	
	/**
	 * @return the variables
	 */
	public List<Variable> getVariables() {
		return variables;
	}

	/**
	 * @param variables the variables to set
	 */
	public void setVariables(List<Variable> variables) {
		this.variables = variables;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((variables == null) ? 0 : variables.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConstraintAllDiff other = (ConstraintAllDiff) obj;
		if (variables == null) {
			if (other.variables != null)
				return false;
		} else if (!variables.equals(other.variables))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "";
	}
}
