package cryptarithme.constraint;

import java.util.List;
import cryptarithme.Variable;

/**
 * Contrainte à appliquer sur les variables
 * 
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public interface Constraint extends Cloneable {
	
	/**
	 * Vérifie si la contrainte est respectée
	 * @return
	 */
	public boolean check();
	
	/**
	 * Changer les variables par les nouvelles variables clonées
	 * @param variables the variables to set
	 */
	public void setVariablesCloned(List<Variable> variablesCloned);
	
	/**
	 * Permet de cloner une contrainte
	 * @return le clone de la contrainte
	 * @throws CloneNotSupportedException
	 */
	public Constraint clone() throws CloneNotSupportedException;
	
	/**
	 * Permet de calculer la fonction h(n) de la contrainte pour l'exploration en A* 
	 * @return le cout estimé du meilleur chemin du noeud n au noeud but
	 */
	public double calculHHeuristic();
}
