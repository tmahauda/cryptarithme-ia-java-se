package cryptarithme;

import java.util.ArrayList;
import java.util.List;
import cryptarithme.constraint.Constraint;

/**
 * Un cryptarithme ou jeu cryptarithmétique est un casse-tête numérique et logique qui consiste en une équation mathématique 
 * où les lettres doivent être remplacées par des chiffres, deux lettres ne pouvant pas avoir la même valeur. 
 * Chaque lettre représente un seul chiffre et le chiffre le plus significatif est différent de zéro
 * L'un des plus connu car probablement le plus ancien est le problème SEND+MORE=MONEY
 * 
 * Plus de détail sur :
 * - http://info.univ-angers.fr/~richer/ensm1i_ia_jeux.php
 * - https://fr.wikipedia.org/wiki/Cryptarithme
 * 
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class Cryptarithme implements Cloneable {
	
	/**
	 * Les variables dont il faut trouver les chiffres
	 */
	private List<Variable> variables;
	
	/**
	 * Les contraintes à respecter sur les variables
	 * - Chaque lettre représente un seul chiffre --> ConstraintAllDiff
	 * - Equation mathématique --> ConstraintEquality
	 */
	private List<Constraint> constraints;
	
	public Cryptarithme(List<Variable> variables, List<Constraint> constraints) {
		this.variables = variables;
		this.constraints = constraints;
	}

	@Override
	public Cryptarithme clone() throws CloneNotSupportedException {
		List<Variable> variables = new ArrayList<>();
		for(Variable variable : this.variables) {
			variables.add(variable.clone());
		}
		
		List<Constraint> constraints = new ArrayList<>();
		for(Constraint constraint : this.constraints) {
			Constraint constraintCloned = constraint.clone();
			constraintCloned.setVariablesCloned(variables);
			constraints.add(constraintCloned);
		}
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * Vérifie si toutes les contraintes sont respectées
	 * @return vrai si respecté. Faux dans le cas contraire
	 */
	public boolean soluceFind() {
		for(Constraint constraint : this.constraints) {
			if(!constraint.check())
				return false;
		}
		return true;
	}
	
	/**
	 * @return the variables
	 */
	public List<Variable> getVariables() {
		return variables;
	}

	/**
	 * @param variables the variables to set
	 */
	public void setVariables(List<Variable> variables) {
		this.variables = variables;
	}

	/**
	 * @return the constraints
	 */
	public List<Constraint> getConstraints() {
		return constraints;
	}

	/**
	 * @param constraints the constraints to set
	 */
	public void setConstraints(List<Constraint> constraints) {
		this.constraints = constraints;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((variables == null) ? 0 : variables.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cryptarithme other = (Cryptarithme) obj;
		if (variables == null) {
			if (other.variables != null)
				return false;
		} else if (!variables.containsAll(other.variables))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for(Variable var : this.variables) {
			sb.append(var.toString());
			sb.append("\n");
		}
		
		return sb.toString();
	}
	
	public String toStringWord() {
		StringBuilder sb = new StringBuilder();
		
		for(Constraint constraint : this.constraints) {
			sb.append(constraint.toString());
		}
		
		return sb.toString();
	}
}
