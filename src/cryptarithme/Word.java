package cryptarithme;

import java.util.ArrayList;
import java.util.List;

/**
 * Mot formé par un ensemble de variables
 *
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class Word implements Cloneable {

	private List<Variable> variables;
	
	public Word(List<Variable> variables) {
		this.variables = variables;
	}

	/**
	 * Exemple avec SEND
	 * S ∗ 1000 + E ∗ 100 + N ∗ 10 + D
	 * @return la somme de chaque variable que compose chaque mot
	 */
	public int sum() {
		int result = 0;
		int position = this.variables.size() - 1;
		
		for(Variable variable : this.variables) {
			result += variable.getValeur() * Math.pow(10, position);
			position--;
		}
		
		return result;
	}
	
	@Override
	public Word clone() throws CloneNotSupportedException {
		List<Variable> variables = new ArrayList<>();
		for(Variable variable : this.variables) {
			variables.add(variable.clone());
		}
		return new Word(variables);
	}

	/**
	 * @return the variables
	 */
	public List<Variable> getVariables() {
		return variables;
	}

	/**
	 * @param variables the variables to set
	 */
	public void setVariables(List<Variable> variables) {
		this.variables = variables;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((variables == null) ? 0 : variables.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Word other = (Word) obj;
		if (variables == null) {
			if (other.variables != null)
				return false;
		} else if (!variables.equals(other.variables))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for(Variable var : this.variables) {
			sb.append(var.getCharacter());
		}
		
		return sb.toString();
	}
}
