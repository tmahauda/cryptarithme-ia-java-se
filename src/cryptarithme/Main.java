package cryptarithme;

import cryptarithme.factory.FactoryCryptarithmeFromArgs;
import cryptarithme.factory.FactoryCryptarithmeFromExamples;
import ia.search.FactorySearch;
import ia.search.Node;

/**
 * Test pour résoudre les problèmes cryptarithme
 *
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class Main {

	public static void main(String[] args) {
		Cryptarithme cryptarithme = FactoryCryptarithmeFromArgs.buildCryptarithmeArgs(args);
		if(cryptarithme == null) {
			cryptarithme = FactoryCryptarithmeFromExamples.buildSendMoreMoney();
		}
		
		System.out.println("Résolution de " + cryptarithme.toStringWord() + " en cours. Veuillez patienter");
		
		Node<CryptarithmeAdapter> soluceCryptarithme = FactorySearch.getSoluceCryptarithme(cryptarithme);
		if(soluceCryptarithme == null) {
			System.out.println("Aucune solution n'a été trouvée");
		} else {
			System.out.println("Une solution est : ");
			System.out.println(soluceCryptarithme.getEtat());	
		}
	}
}
