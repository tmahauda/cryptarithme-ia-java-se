package cryptarithme.factory;

import java.util.Arrays;
import java.util.List;
import cryptarithme.Cryptarithme;
import cryptarithme.Domain;
import cryptarithme.Variable;
import cryptarithme.Word;
import cryptarithme.constraint.Constraint;
import cryptarithme.constraint.ConstraintAllDiff;
import cryptarithme.constraint.ConstraintEquality;

/**
 * Crée différents cryptarithme issus de : 
 * - Wikipédia : https://fr.wikipedia.org/wiki/Cryptarithme
 * - Graner : https://www.graner.net/nicolas/nombres/crypt.php
 * Lorsqu'on affecte uniquement la première variable à 9, la résolution est plus rapide. Pourquoi ?
 *
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class FactoryCryptarithmeFromExamples {

	private static final Domain DOMAIN_0TO9 = new Domain(0, 9, 1);
	private static final Domain DOMAIN_1TO9 = new Domain(1, 9, 1);
	
	private FactoryCryptarithmeFromExamples() {
		
	}
	
	/**
	 * Résolution OK mois de 1min
	 * @return SEND + MORE = MONEY (9567 + 1085 = 10652)
	 */
	public static Cryptarithme buildSendMoreMoney() {
		Variable s = new Variable('S', DOMAIN_1TO9, 9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable n = new Variable('N', DOMAIN_0TO9);
		Variable d = new Variable('D', DOMAIN_0TO9);
		Variable m = new Variable('M', DOMAIN_1TO9);
		Variable o = new Variable('O', DOMAIN_0TO9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		Variable y = new Variable('Y', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(s, e, n, d, m, o, r, y);
		
		Word send = new Word(Arrays.asList(s, e, n, d));
		Word more = new Word(Arrays.asList(m, o, r, e));
		Word money = new Word(Arrays.asList(m, o, n, e, y));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(send, more), Arrays.asList(money));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * Résolution OK mois de 1min
	 * @return COCA + COLA = PEPSI (9093 + 9053 = 18146)
	 */
	public static Cryptarithme buildCocaColaPepsi() {
		Variable c = new Variable('C', DOMAIN_1TO9, 9);
		Variable o = new Variable('O', DOMAIN_0TO9);
		Variable a = new Variable('A', DOMAIN_0TO9);
		Variable l = new Variable('L', DOMAIN_0TO9);
		Variable p = new Variable('P', DOMAIN_1TO9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable s = new Variable('S', DOMAIN_0TO9);
		Variable i = new Variable('I', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(c, o, a, l, p, e, s, i);
		
		Word coca = new Word(Arrays.asList(c, o, c, a));
		Word cola = new Word(Arrays.asList(c, o, l, a));
		Word pepsi = new Word(Arrays.asList(p, e, p, s, i));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(coca, cola), Arrays.asList(pepsi));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * Résolution OK mois de 1min
	 * @return MANGER + MANGER = GROSSIR (536140 + 536140 = 1072280)
	 */
	public static Cryptarithme buildMangerMangerGrossir() {
		Variable m = new Variable('M', DOMAIN_1TO9, 9);
		Variable a = new Variable('A', DOMAIN_0TO9);
		Variable n = new Variable('N', DOMAIN_0TO9);
		Variable g = new Variable('G', DOMAIN_1TO9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		Variable o = new Variable('O', DOMAIN_0TO9);
		Variable s = new Variable('S', DOMAIN_0TO9);
		Variable i = new Variable('I', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(m, a, n, g, e, r, o, s, i);
		
		Word manger = new Word(Arrays.asList(m, a, n, g, e, r));
		Word grossir = new Word(Arrays.asList(g, r, o, s, s, i, r));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(manger, manger), Arrays.asList(grossir));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * Résolution OK mois de 1min
	 * @return ALCOOL + ALCOOL = IVRESSE (927332 + 927332 = 1854665)
	 */
	public static Cryptarithme buildAlcoolAlcoolIvresse() {
		Variable a = new Variable('A', DOMAIN_1TO9, 9);
		Variable l = new Variable('L', DOMAIN_0TO9);
		Variable c = new Variable('C', DOMAIN_0TO9);
		Variable o = new Variable('O', DOMAIN_0TO9);
		Variable i = new Variable('I', DOMAIN_1TO9);
		Variable v = new Variable('V', DOMAIN_0TO9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable s = new Variable('S', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(a, l, c, o, i, v, r, e, s);
		
		Word alcool = new Word(Arrays.asList(a, l, c, o, o, l));
		Word ivresse = new Word(Arrays.asList(i, v, r, e, s, s, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(alcool, alcool), Arrays.asList(ivresse));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * Résolution KO (trop long après 10min)
	 * @return LECON + ELEVE = DEVOIR ?
	 */
	public static Cryptarithme buildLeconEleveDevoir() {
		Variable l = new Variable('L', DOMAIN_1TO9, 9);
		Variable e = new Variable('E', DOMAIN_1TO9);
		Variable c = new Variable('C', DOMAIN_0TO9);
		Variable o = new Variable('O', DOMAIN_0TO9);
		Variable n = new Variable('N', DOMAIN_0TO9);
		Variable v = new Variable('V', DOMAIN_0TO9);
		Variable d = new Variable('D', DOMAIN_1TO9);
		Variable i = new Variable('I', DOMAIN_0TO9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(l, e, c, o, n, v, d, i, r);
		
		Word lecon = new Word(Arrays.asList(l, e, c, o, n));
		Word eleve = new Word(Arrays.asList(e, l, e, v, e));
		Word devoir = new Word(Arrays.asList(d, e, v, o, i, r));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(lecon, eleve), Arrays.asList(devoir));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * Résolution OK en mois de 1min
	 * @return KILO + KILO = TONNE (9178 + 9178 = 18556)
	 */
	public static Cryptarithme buildKiloKiloTonne() {
		Variable k = new Variable('K', DOMAIN_1TO9, 9);
		Variable i = new Variable('I', DOMAIN_0TO9);
		Variable l = new Variable('L', DOMAIN_0TO9);
		Variable o = new Variable('O', DOMAIN_0TO9);
		Variable t = new Variable('T', DOMAIN_1TO9);
		Variable n = new Variable('N', DOMAIN_0TO9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(k, i, l, o, t, n, e);
		
		Word kilo = new Word(Arrays.asList(k, i, l, o));
		Word tonne = new Word(Arrays.asList(t, o, n, n, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(kilo, kilo), Arrays.asList(tonne));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * Résolution KO (trop long après 10min)
	 * @return CHEVAL + VACHE = OISEAU ?
	 */
	public static Cryptarithme buildChevalVacheOiseau() {
		Variable c = new Variable('C', DOMAIN_1TO9, 9);
		Variable h = new Variable('H', DOMAIN_0TO9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable v = new Variable('V', DOMAIN_1TO9);
		Variable a = new Variable('A', DOMAIN_0TO9);
		Variable l = new Variable('L', DOMAIN_0TO9);
		Variable o = new Variable('O', DOMAIN_1TO9);
		Variable i = new Variable('I', DOMAIN_0TO9);
		Variable s = new Variable('S', DOMAIN_0TO9);
		Variable u = new Variable('U', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(c, h, e, v, a, l, o, i, s, u);
		
		Word cheval = new Word(Arrays.asList(c, h, e, v, a, l));
		Word vache = new Word(Arrays.asList(v, a, c, h, e));
		Word oiseau = new Word(Arrays.asList(o, i, s, e, a, u));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(cheval, vache), Arrays.asList(oiseau));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	
	/**
	 * Exemples arithmétique de Wikipédia : https://fr.wikipedia.org/wiki/Cryptarithme
	 */
	
	/**
	 * @return HUIT + HUIT = SEIZE (8253 + 8253 = 16506 ou 9254 + 9254 = 18508)
	 */
	public static Cryptarithme buildHuitHuitSeize() {
		Variable h = new Variable('H', DOMAIN_1TO9, 9);
		Variable u = new Variable('U', DOMAIN_0TO9);
		Variable i = new Variable('I', DOMAIN_0TO9);
		Variable t = new Variable('T', DOMAIN_0TO9);
		Variable s = new Variable('S', DOMAIN_1TO9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable z = new Variable('Z', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(h, u, i, t, s, e, z);
		
		Word huit = new Word(Arrays.asList(h, u, i, t));
		Word seize = new Word(Arrays.asList(s, e, i, z, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(huit, huit), Arrays.asList(seize));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * @return UN + UN + NEUF = ONZE (81 + 81 + 1987 = 2149)
	 */
	public static Cryptarithme buildUnUnNeufOnze() {
		Variable u = new Variable('U', DOMAIN_1TO9, 9);
		Variable n = new Variable('N', DOMAIN_1TO9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable f = new Variable('F', DOMAIN_0TO9);
		Variable o = new Variable('O', DOMAIN_1TO9);
		Variable z = new Variable('Z', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(u, n, e, f, o, z);
		
		Word un = new Word(Arrays.asList(u, n));
		Word neuf = new Word(Arrays.asList(n, e, u, f));
		Word onze = new Word(Arrays.asList(o, n, z, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(un, un, neuf), Arrays.asList(onze));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * @return CINQ + CINQ + VINGT = TRENTE (6483 + 6483 + 94851 = 107817)
	 */
	public static Cryptarithme buildCinqCinqVingtTrente() {
		Variable c = new Variable('C', DOMAIN_1TO9, 9);
		Variable i = new Variable('I', DOMAIN_0TO9);
		Variable n = new Variable('N', DOMAIN_0TO9);
		Variable q = new Variable('Q', DOMAIN_0TO9);
		Variable v = new Variable('V', DOMAIN_1TO9);
		Variable g = new Variable('G', DOMAIN_0TO9);
		Variable t = new Variable('T', DOMAIN_1TO9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(c, i, n, q, v, g, t, r, e);
		
		Word cinq = new Word(Arrays.asList(c, i, n, q));
		Word vingt = new Word(Arrays.asList(v, i, n, g, t));
		Word trente = new Word(Arrays.asList(t, r, e, n, t, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(cinq, cinq, vingt), Arrays.asList(trente));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * @return ZERO + NEUF + NEUF + DOUZE = TRENTE (9206 + 3257 + 3257 + 86592 = 102312)
	 */
	public static Cryptarithme buildZeroNeufNeufDouzeTrente() {
		Variable z = new Variable('Z', DOMAIN_1TO9, 9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		Variable o = new Variable('O', DOMAIN_0TO9);
		Variable n = new Variable('N', DOMAIN_1TO9);
		Variable u = new Variable('U', DOMAIN_0TO9);
		Variable f = new Variable('F', DOMAIN_0TO9);
		Variable d = new Variable('D', DOMAIN_1TO9);
		Variable t = new Variable('T', DOMAIN_1TO9);
		
		List<Variable> variables = Arrays.asList(z, e, r, o, n, u, f, d, t);
		
		Word zero = new Word(Arrays.asList(z, e, r, o));
		Word neuf = new Word(Arrays.asList(n, e, u, f));
		Word douze = new Word(Arrays.asList(d, o, u, z, e));
		Word trente = new Word(Arrays.asList(t, r, e, n, t, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(zero, neuf, neuf, douze), Arrays.asList(trente));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * @return ZERO + ZERO + ZERO + UN + DOUZE = TREIZE (9506 + 9506 + 9506 + 82 + 76895 = 105495) 
	 */
	public static Cryptarithme buildZeroZeroZeroUnDouzeTreize() {
		Variable z = new Variable('Z', DOMAIN_1TO9, 9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		Variable o = new Variable('O', DOMAIN_0TO9);
		Variable u = new Variable('U', DOMAIN_1TO9);
		Variable n = new Variable('N', DOMAIN_0TO9);
		Variable d = new Variable('D', DOMAIN_1TO9);
		Variable t = new Variable('T', DOMAIN_1TO9);
		Variable i = new Variable('I', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(z, e, r, o, u, n, d, t, i);
		
		Word zero = new Word(Arrays.asList(z, e, r, o));
		Word un = new Word(Arrays.asList(u, n));
		Word douze = new Word(Arrays.asList(d, o, u, z, e));
		Word treize = new Word(Arrays.asList(t, r, e, i, z, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(zero, zero, zero, un, douze), Arrays.asList(treize));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * @return ZERO + ZERO + SEPT + SEPT + SEIZE = TRENTE (6904 + 6904 + 7921 + 7921 + 79869 = 109519)
	 */
	public static Cryptarithme buildZeroZeroSeptSeptSeizeTrente() {
		Variable z = new Variable('Z', DOMAIN_1TO9, 9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		Variable o = new Variable('O', DOMAIN_0TO9);
		Variable s = new Variable('S', DOMAIN_1TO9);
		Variable p = new Variable('P', DOMAIN_0TO9);
		Variable t = new Variable('T', DOMAIN_1TO9);
		Variable i = new Variable('I', DOMAIN_0TO9);
		Variable n = new Variable('N', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(z, e, r, o, s, p, t, i, n);
		
		Word zero = new Word(Arrays.asList(z, e, r, o));
		Word sept = new Word(Arrays.asList(s, e, p, t));
		Word seize = new Word(Arrays.asList(s, e, i, z, e));
		Word trente = new Word(Arrays.asList(t, r, e, n, t, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(zero, zero, sept, sept, seize), Arrays.asList(trente));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * @return ZERO + UN + TROIS + ONZE + QUINZE = TRENTE (7139 + 68 + 53902 + 9871 + 460871 = 531851) 
	 */
	public static Cryptarithme buildZeroUnTroisOnzeQuinzeTrente() {
		Variable z = new Variable('Z', DOMAIN_1TO9, 9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		Variable o = new Variable('O', DOMAIN_1TO9);
		Variable u = new Variable('U', DOMAIN_1TO9);
		Variable n = new Variable('N', DOMAIN_0TO9);
		Variable t = new Variable('T', DOMAIN_1TO9);
		Variable i = new Variable('I', DOMAIN_0TO9);
		Variable s = new Variable('S', DOMAIN_0TO9);
		Variable q = new Variable('Q', DOMAIN_1TO9);
		
		List<Variable> variables = Arrays.asList(z, e, r, o, u, n, t, i, s, q);
		
		Word zero = new Word(Arrays.asList(z, e, r, o));
		Word un = new Word(Arrays.asList(u, n));
		Word trois = new Word(Arrays.asList(t, r, o, i, s));
		Word onze = new Word(Arrays.asList(o, n, z, e));
		Word quinze = new Word(Arrays.asList(q, u, i, n, z, e));
		Word trente = new Word(Arrays.asList(t, r, e, n, t, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(zero, un, trois, onze, quinze), Arrays.asList(trente));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * @return ZERO + TROIS + TROIS + TROIS + SEPT = SEIZE (4273 + 17356 + 17356 + 17356 + 6201 = 62542)
	 */
	public static Cryptarithme buildZeroTroisTroisTroisSeptSeize() {
		Variable z = new Variable('Z', DOMAIN_1TO9, 9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		Variable o = new Variable('O', DOMAIN_0TO9);
		Variable t = new Variable('T', DOMAIN_1TO9);
		Variable i = new Variable('I', DOMAIN_0TO9);
		Variable s = new Variable('S', DOMAIN_1TO9);
		Variable p = new Variable('P', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(z, e, r, o, t, i, s, p);
		
		Word zero = new Word(Arrays.asList(z, e, r, o));
		Word trois = new Word(Arrays.asList(t, r, o, i, s));
		Word sept = new Word(Arrays.asList(s, e, p, t));
		Word seize = new Word(Arrays.asList(s, e, i, z, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(zero, trois, trois, trois, sept), Arrays.asList(seize));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * @return ZERO + TROIS + TROIS + DOUZE + DOUZE = TRENTE (3496 + 19625 + 19625 + 76034 + 76034 = 194814)
	 */
	public static Cryptarithme buildZeroTroisTroisDouzeDouzeTrente() {
		Variable z = new Variable('Z', DOMAIN_1TO9, 9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		Variable o = new Variable('O', DOMAIN_0TO9);
		Variable t = new Variable('T', DOMAIN_1TO9);
		Variable i = new Variable('I', DOMAIN_0TO9);
		Variable s = new Variable('S', DOMAIN_0TO9);
		Variable d = new Variable('D', DOMAIN_1TO9);
		Variable u = new Variable('U', DOMAIN_0TO9);
		Variable n = new Variable('N', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(z, e, r, o, t, i, s, d, u, n);
		
		Word zero = new Word(Arrays.asList(z, e, r, o));
		Word trois = new Word(Arrays.asList(t, r, o, i, s));
		Word douze = new Word(Arrays.asList(d, o, u, z, e));
		Word trente = new Word(Arrays.asList(t, r, e, n, t, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(zero, trois, trois, douze, douze), Arrays.asList(trente));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * Résolution OK en moins de 10 min
	 * @return ZERO + QUATRE + QUATRE + ONZE + ONZE = TRENTE (4876 + 130278 + 130278 + 6548 + 6548 = 278528) 
	 */
	public static Cryptarithme buildZeroQuatreQuatreOnzeOnzeTrente() {
		Variable z = new Variable('Z', DOMAIN_1TO9, 9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		Variable o = new Variable('O', DOMAIN_1TO9);
		Variable q = new Variable('Q', DOMAIN_1TO9);
		Variable u = new Variable('U', DOMAIN_0TO9);
		Variable a = new Variable('A', DOMAIN_0TO9);
		Variable t = new Variable('T', DOMAIN_1TO9);
		Variable n = new Variable('N', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(z, e, r, o, q, u, a, t, n);
		
		Word zero = new Word(Arrays.asList(z, e, r, o));
		Word quatre = new Word(Arrays.asList(q, u, a, t, r, e));
		Word onze = new Word(Arrays.asList(o, n, z, e));
		Word trente = new Word(Arrays.asList(t, r, e, n, t, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(zero, quatre, quatre, onze, onze), Arrays.asList(trente));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * Résolution KO (trop long après 10min)
	 * @return UN + UN + QUATRE + DOUZE + DOUZE = TRENTE (59 + 59 + 652801 + 74531 + 74531 = 801981)
	 */
	public static Cryptarithme buildUnUnQuatreDouzeDouzeTrente() {
		Variable u = new Variable('U', DOMAIN_1TO9, 9);
		Variable n = new Variable('N', DOMAIN_0TO9);
		Variable q = new Variable('Q', DOMAIN_1TO9);
		Variable a = new Variable('A', DOMAIN_0TO9);
		Variable t = new Variable('T', DOMAIN_1TO9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable d = new Variable('D', DOMAIN_1TO9);
		Variable o = new Variable('O', DOMAIN_0TO9);
		Variable z = new Variable('Z', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(u, n, q, a, t, r, e, d, o, z);
		
		Word un = new Word(Arrays.asList(u, n));
		Word quatre = new Word(Arrays.asList(q, u, a, t, r, e));
		Word douze = new Word(Arrays.asList(d, o, u, z, e));
		Word trente = new Word(Arrays.asList(t, r, e, n, t, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(un, un, quatre, douze, douze), Arrays.asList(trente));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * Résolution OK en moins de 1min
	 * @return UN + DEUX + DEUX + DEUX + DEUX = NEUF (25 + 1326 + 1326 + 1326 + 1326 = 5329) 
	 */
	public static Cryptarithme buildUnDeuxDeuxDeuxDeuxNeuf() {
		Variable u = new Variable('U', DOMAIN_1TO9, 9);
		Variable n = new Variable('N', DOMAIN_1TO9);
		Variable d = new Variable('D', DOMAIN_1TO9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable x = new Variable('X', DOMAIN_0TO9);
		Variable f = new Variable('F', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(u, n, d, e, x, f);
		
		Word un = new Word(Arrays.asList(u, n));
		Word deux = new Word(Arrays.asList(d, e, u, x));
		Word neuf = new Word(Arrays.asList(n, e, u, f));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(un, deux, deux, deux, deux), Arrays.asList(neuf));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * Résolution KO (trop long après 10min)
	 * @return UN + QUATRE + CINQ + CINQ + QUINZE = TRENTE (50 + 356724 + 8103 + 8103 + 351094 = 724074) 
	 */
	public static Cryptarithme buildUnQuatreCinqCinqQuinzeTrente() {
		Variable u = new Variable('U', DOMAIN_1TO9, 9);
		Variable n = new Variable('N', DOMAIN_0TO9);
		Variable q = new Variable('Q', DOMAIN_1TO9);
		Variable a = new Variable('A', DOMAIN_0TO9);
		Variable t = new Variable('T', DOMAIN_1TO9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable c = new Variable('C', DOMAIN_1TO9);
		Variable i = new Variable('I', DOMAIN_0TO9);
		Variable z = new Variable('Z', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(u, n, q, a, t, r, e, c, i, z);
		
		Word un = new Word(Arrays.asList(u, n));
		Word quatre = new Word(Arrays.asList(q, u, a, t, r, e));
		Word cinq = new Word(Arrays.asList(c, i, n, q));
		Word quinze = new Word(Arrays.asList(q, u, i, n, z, e));
		Word trente = new Word(Arrays.asList(t, r, e, n, t, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(un, quatre, cinq, cinq, quinze), Arrays.asList(trente));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * Résolution OK en moins de 1min
	 * @return TROIS + TROIS + TROIS + CINQ + SEIZE = TRENTE (14509 + 14509 + 14509 + 7063 + 98028 = 148618) 
	 */
	public static Cryptarithme buildTroisTroisTroisCinqSeizeTrente() {
		Variable t = new Variable('T', DOMAIN_1TO9, 9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		Variable o = new Variable('O', DOMAIN_0TO9);
		Variable i = new Variable('I', DOMAIN_0TO9);
		Variable s = new Variable('S', DOMAIN_1TO9);
		Variable c = new Variable('C', DOMAIN_1TO9);
		Variable n = new Variable('N', DOMAIN_0TO9);
		Variable q = new Variable('Q', DOMAIN_0TO9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable z = new Variable('Z', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(t, r, o, i, s, c, n, q, e, z);
		
		Word trois = new Word(Arrays.asList(t, r, o, i, s));
		Word cinq = new Word(Arrays.asList(c, i, n, q));
		Word seize = new Word(Arrays.asList(s, e, i, z, e));
		Word trente = new Word(Arrays.asList(t, r, e, n, t, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(trois, trois, trois, cinq, seize), Arrays.asList(trente));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * Résolution KO (trop long après 10min)
	 * @return QUATRE + QUATRE + QUATRE + NEUF + NEUF = TRENTE (172536 + 172536 + 172536 + 9674 + 9674 = 536956)
	 */
	public static Cryptarithme buildQuatreQuatreQuatreNeufNeufTrente() {
		Variable q = new Variable('Q', DOMAIN_1TO9, 9);
		Variable u = new Variable('U', DOMAIN_0TO9);
		Variable a = new Variable('A', DOMAIN_0TO9);
		Variable t = new Variable('T', DOMAIN_1TO9);
		Variable r = new Variable('R', DOMAIN_0TO9);
		Variable e = new Variable('E', DOMAIN_0TO9);
		Variable n = new Variable('N', DOMAIN_1TO9);
		Variable f = new Variable('F', DOMAIN_0TO9);
		
		List<Variable> variables = Arrays.asList(q, u, a, t, r, e, n, f);
		
		Word quatre = new Word(Arrays.asList(q, u, a, t, r, e));
		Word neuf = new Word(Arrays.asList(n, e, u, f));
		Word trente = new Word(Arrays.asList(t, r, e, n, t, e));
		
		Constraint equality = new ConstraintEquality(Arrays.asList(quatre, quatre, quatre, neuf, neuf), Arrays.asList(trente));
		Constraint allDiff = new ConstraintAllDiff(variables);
		
		List<Constraint> constraints = Arrays.asList(equality, allDiff);
		
		return new Cryptarithme(variables, constraints);
	}
}
