package cryptarithme.factory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import cryptarithme.Cryptarithme;
import cryptarithme.Domain;
import cryptarithme.Variable;
import cryptarithme.Word;
import cryptarithme.constraint.Constraint;
import cryptarithme.constraint.ConstraintAllDiff;
import cryptarithme.constraint.ConstraintEquality;

/**
 * Construit un problème cryptarithme à partir des arguments fournis dans le terminal
 * 
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class FactoryCryptarithmeFromArgs {

	private static final Domain DOMAIN_0TO9 = new Domain(0, 9, 1);
	private static final Domain DOMAIN_1TO9 = new Domain(1, 9, 1);
	
	private FactoryCryptarithmeFromArgs() {
		
	}
	
	/**
	 * Construit un problème cryptarithme à partir des arguments fournis dans le terminal
	 * @param args qui contient les variables à résoudre
	 * @return le problème cryptarithme construit
	 */
	public static Cryptarithme buildCryptarithmeArgs(String[] args) {
		//On vérifie si les arguments sont renseignés correctement
		if(!checkArgs(args)) return null;
		
		//On récupère la liste de tous les charactères de chaque argument
		List<Variable> variables = buildVariablesArgs(args);
		if(variables == null || variables.isEmpty()) {
			System.out.println("Problèmes lors de la construction des variables.");
			System.out.println("Un exemple sera exécuté à la place");
			return null;
		}
		
		//On construit les mots de la première parties
		List<Word> firstWords = buildWordsArgs(args, variables, 0, args.length - 1);
		if(firstWords == null || firstWords.isEmpty()) {
			System.out.println("Problèmes lors de la construction des premiers mots.");
			System.out.println("Un exemple sera exécuté à la place");
			return null;
		}
		
		//On construit le mot de la dernière partie
		List<Word> lastWords = buildWordsArgs(args, variables, args.length - 1, args.length);
		if(lastWords == null || lastWords.isEmpty()) {
			System.out.println("Problèmes lors de la construction du dernier mot.");
			System.out.println("Un exemple sera exécuté à la place");
			return null;
		}
		
		//On construit les contraintes
		List<Constraint> constraints = buildConstraintsArgs(variables, firstWords, lastWords);
		if(constraints == null || constraints.isEmpty()) {
			System.out.println("Problèmes lors de la construction des contraintes.");
			System.out.println("Un exemple sera exécuté à la place");
			return null;
		}
		
		return new Cryptarithme(variables, constraints);
	}
	
	/**
	 * Vérifie si les arguments sont correctement renseignés
	 * @param args
	 * @return
	 */
	private static boolean checkArgs(String[] args) {
		
		//Au moins deux arguments est attendu
		if(args == null || args.length < 2) {
			System.out.println("Aucun argument n'est fourni ou vous n'avez pas fournis au moins deux arguments !");
			System.out.println("Un exemple sera exécuté à la place");
			return false;
		}
		
		//Chaque argument ne doit contenir uniquement des lettre alphabétique de a à z
		for(String arg : args) {
			if(!Pattern.matches("^[a-zA-Z]+$", arg)) {
				System.out.println("L'argument " + arg + " ne respecte pas le format. Uniquement des lettres alphabétiques !");
				System.out.println("Un exemple sera exécuté à la place");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Construit une liste de variables sans doublon à partir des arguments
	 * @param args
	 * @return
	 */
	private static List<Variable> buildVariablesArgs(String[] args) {
		List<Variable> variables = new ArrayList<>();
		boolean firstCharBegin = true;
		boolean firstCharArg = false;
		
		for(String arg : args) {
			//On met le premier caractere à true de l'argument
			firstCharArg = true;
			
			//On met tous les caractères en majuscule et on supprime les éventuels espaces blancs
			arg = arg.toUpperCase().trim();
			
			//On récupère le tableau de caracteres
			char[] chars = arg.toCharArray();
			
			//On ajoute les caractères dans la liste sans doublon
			for(char c : chars) {
				Variable var = null;
				
				//Si c'est le tout premier caractere dans ce cas on lui affecte la valeur 9
				//avec un domaine de 1 à 9
				if(firstCharBegin) {
					var = new Variable(c, DOMAIN_1TO9, 9);
					firstCharBegin = false;
					firstCharArg = false;
				}
				//Si c'est le premier caractere de l'argument, domaine 1 à 9
				//Le chiffre le plus significatif est différent de zéro
				else if(firstCharArg) {
					var = new Variable(c, DOMAIN_1TO9);
					firstCharBegin = false;
					firstCharArg = false;
				}
				//Sinon domaine de 0 à 9
				else {
					var = new Variable(c, DOMAIN_0TO9);
				}
				
				//Si la variable est déjà présente
				Variable varExist = getVariableInList(variables, c);
				if(varExist != null) {
					//Mais que le domaine de définition est différent dans ce cas
					//c'est le domaine de 1 à 9 qui est prioritaire
					if(!varExist.getDomain().equals(var.getDomain())) {
						varExist.setDomain(DOMAIN_1TO9);
						if(varExist.getValeur() != 9) {
							varExist.setValeur(1);
						}
					}
				}
				//Sinon on l'ajoute dans la liste
				else {
					variables.add(var);
				}
			}
		}
		
		return variables;
	}
	
	/**
	 * Vérifier si une variable est déjà présent dans la liste
	 * @param variables la liste
	 * @param c la variable à vérifier dans la liste
	 * @return vrai la variable c est déjà présent dans la liste. Faux dans le cas contraire
	 */
	private static Variable getVariableInList(List<Variable> variables, Character c) {
		for(Variable variable : variables) {
			if(variable.getCharacter().equals(c)) {
				return variable;
			}
		}
		return null;
	}
	
	/**
	 * Construit une liste de mots sans doublon à partir des variables
	 * @param args
	 * @param variables
	 * @param start
	 * @param end
	 * @return
	 */
	private static List<Word> buildWordsArgs(String[] args, List<Variable> variables, int start, int end) {
		List<Word> words = new ArrayList<>();
		
		for(int i=start; i<end; i++) {
			//On met tous les caractères en majuscule et on supprime les éventuels espaces blancs
			String arg = args[i].toUpperCase().trim();
			
			//On récupère le tableau de caracteres
			char[] chars = arg.toCharArray();
			
			List<Variable> variablesWord = new ArrayList<Variable>();
			
			for(char c : chars) {
				Variable var = getVariableInList(variables, c);
				variablesWord.add(var);
			}
			
			Word word = new Word(variablesWord);
			words.add(word);
		}
		
		return words;
	}
	
	/**
	 * Construit une liste de contraintes sans doublon à partir des variables et des mots
	 * @param variables
	 * @param firstWords
	 * @param lastWords
	 * @return
	 */
	private static List<Constraint> buildConstraintsArgs(List<Variable> variables, List<Word> firstWords, List<Word> lastWords) {
		List<Constraint> constraints = new ArrayList<>();
		
		Constraint allDiff = new ConstraintAllDiff(variables);
		Constraint equality = new ConstraintEquality(firstWords, lastWords);
		
		constraints.add(equality);
		constraints.add(allDiff);
		
		return constraints;
	}
}
