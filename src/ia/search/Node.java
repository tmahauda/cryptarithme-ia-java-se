package ia.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import ia.solve.Solveable;
import ia.solve.heuristic.Heuristic;

 /**
 * La classe Noeud implémente l'interface Comparable pour la structure de données file prioritaire
 * permettant de créer les états de la situation
 * Il permet le parcours en arbre
 * @see Comparable
 * 
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class Node<T extends Solveable> implements Comparable<Node<T>> {
	
	/**
	 * Total nombre de noeud crée
	 */
	private static int totalNodeCreate = 0;
	
	/**
     * L'état de l'espace des états auquel le noeud correspond
     */
	private T etat;
	
	/**
     * Le noeud de l'arbre de recherche qui a généré ce noeud
     */
	private Node<T> parent;
	
	/**
    * Les enfants de ce noeud
    */
	private List<Node<T>> childrens;
	
	/**
     * L'action qui a été appliquée au parent pour générer le noeud
     */
	private String action;
	
	/**
     * Le coût du chemin de la racine de l'arbre jusqu'au noeud
     */
	private int cout;
	
	/**
     * Le nombre d'étapes que compte le chemin de la racine jusqu'au noeud
     */
	private int profondeur;
	
	/**
	 * La fonction heuristique utilisé pour le parcours en A*
	 */
	private Heuristic<T> heuristic;
	
	private String valuesHeuristic;
	
	/**
	 * Constructeur qui permet d'instancier les attributs de la classe
	 * @param etat
	 * @param parent
	 * @param action
	 * @param cout
	 * @param profondeur
	 * @param heuristic
	 */
	public Node(T etat, Node<T> parent, String action, int cout, int profondeur, Heuristic<T> heuristic) {
		this.etat = etat;
		this.parent = parent;
		this.action = action;
		this.cout = cout;
		this.profondeur = profondeur;
		this.heuristic = heuristic;
		this.heuristic.calculFHeuristic(this);
		this.valuesHeuristic = this.heuristic.toString();
		this.childrens = new ArrayList<>();
		if(parent != null) {
			parent.getChildrens().add(this);
		}
		totalNodeCreate++;
	}

	/**
	 * @return the etat
	 */
	public T getEtat() {
		return etat;
	}

	/**
	 * @param etat the etat to set
	 */
	public void setEtat(T etat) {
		this.etat = etat;
	}

	/**
	 * @return the parent
	 */
	public Node<T> getParent() {
		return parent;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(Node<T> parent) {
		this.parent = parent;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the cout
	 */
	public int getCout() {
		return cout;
	}

	/**
	 * @param cout the cout to set
	 */
	public void setCout(int cout) {
		this.cout = cout;
	}

	/**
	 * @return the profondeur
	 */
	public int getProfondeur() {
		return profondeur;
	}

	/**
	 * @param profondeur the profondeur to set
	 */
	public void setProfondeur(int profondeur) {
		this.profondeur = profondeur;
	}

	/**
	 * @return the heuristic
	 */
	public Heuristic<T> getHeuristic() {
		return heuristic;
	}

	/**
	 * @param heuristic the heuristic to set
	 */
	public void setHeuristic(Heuristic<T> heuristic) {
		this.heuristic = heuristic;
	}

	/**
	 * @return the childrens
	 */
	public List<Node<T>> getChildrens() {
		return childrens;
	}

	/**
	 * @param childrens the childrens to set
	 */
	public void setChildrens(List<Node<T>> childrens) {
		this.childrens = childrens;
	}

	public LinkedList<Node<T>> getAllNodes() {
		if(this.parent != null) {
			LinkedList<Node<T>> nodes = this.parent.getAllNodes();
			if(!nodes.contains(this)) {
				nodes.add(this);
			}
			return nodes;
		}
		else return new LinkedList<>(Arrays.asList(this));
	}
	
	@Override
	public String toString() {
		String noeudParent = "";
		if(this.parent != null) noeudParent = this.parent.toString();
		String etat = "\n" + this.etat.toString() + "\n";
		String heuristique = "Valeurs heuristique : " + this.valuesHeuristic + "\n";
		String profondeur = "Pronfondeur : " + this.profondeur + "\n";
		String action = "Opérateur : " + this.action + "\n";
		String cout = "Cout : " + String.valueOf(this.cout) + "\n";
		
		return noeudParent+etat+heuristique+profondeur+action+cout;
	}
	
	public String getChemin() {
		String chemin = "";
		if(this.parent != null) chemin = this.parent.getChemin() + " ; " + this.action;
		
		return chemin;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Node))
			return false;
		Node<T> other = (Node<T>)obj;
		if (this.getEtat().equals(other.getEtat())) {
			return true;
		}
		else return false;
	}
	
	/**
     * Méthode qui permet de trier une file prioritaire
     * - Si le noeud courant à une valeur plus importante qu'un autre noeud alors il est prioraire
     * - En cas d'égalité, si le noeud courant est plus proche de la solution, alors il est prioritaire
     * - Sinon il n'est pas prioritaire
     * @param n le noeud à comparer
     * @return action
     */
	@Override
	public int compareTo(Node<T> n) {
		if(this.heuristic.calculFHeuristic(this) > this.heuristic.calculFHeuristic(n)) return 1;
		else if(this.heuristic.calculFHeuristic(this) == this.heuristic.calculFHeuristic(n)) {
			if(this.heuristic.calculHHeuristic(this) > this.heuristic.calculHHeuristic(n)) return 1;
			else if(this.heuristic.calculHHeuristic(this) == this.heuristic.calculHHeuristic(n)) return 0;
			else return -1;
		}
		else return -1;
	}

	/**
	 * @return the totalNodeCreate
	 */
	public static int getTotalNodeCreate() {
		return totalNodeCreate;
	}

	/**
	 * @param totalNodeCreate the totalNodeCreate to set
	 */
	public static void setTotalNodeCreate(int totalNodeCreate) {
		Node.totalNodeCreate = totalNodeCreate;
	}
}
