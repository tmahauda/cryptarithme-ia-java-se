package ia.search;

import cryptarithme.Cryptarithme;
import cryptarithme.CryptarithmeAdapter;
import ia.search.explorable.Explorable;
import ia.search.explorable.ExplorationA;
import ia.solve.Problem;
import ia.solve.ProblemCryptarithme;
import ia.solve.heuristic.Heuristic;
import ia.solve.heuristic.HeuristicCryptarithme;

/**
 * Frabriquer l'arbre de recherche pour résoudre un problème cryptarithme
 *
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class FactorySearch {

	private static final Explorable<CryptarithmeAdapter> FRONTIER = new ExplorationA<>();
	private static final Heuristic<CryptarithmeAdapter> HEURISTIC = new HeuristicCryptarithme();
	
	/**
	 * Constructeur privé afin d'éviter l'instanciation
	 */
	private FactorySearch() {
		
	}
	
	/**
	 * Construit l'arbre de recherche pour résoudre un problème cryptarithme
	 * @param cryptarithme
	 * @param frontier
	 * @param heuristic
	 * @return
	 */
	public static Node<CryptarithmeAdapter> getSoluceCryptarithme(Cryptarithme cryptarithme) {
		//Etat initial
		CryptarithmeAdapter cryptarithmeAdapt = new CryptarithmeAdapter(cryptarithme);
		
		//Pas d'états finaux à définir car on ne les connait pas
		
		//Construction du probleme
		Problem<CryptarithmeAdapter> cryptarithmeProblem = new ProblemCryptarithme(cryptarithmeAdapt);
		
		//Recherche du problème
		Search<CryptarithmeAdapter> cryptarithmeSearch = new Search<>(cryptarithmeProblem, FRONTIER, HEURISTIC);
		
		//Retourne la solution si trouvé
		return cryptarithmeSearch.explore();
	}
}
