package ia.search.explorable;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import ia.search.Node;
import ia.solve.Solveable;

/**
 * Exploration avec une pile
 * Une pile est une liste d’éléments dans laquelle tout insertion et toute
 * suppression se fait à la même extrémité de la pile, appelée sommet
 * 
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class ExplorationProfondeur<T extends Solveable> implements Explorable<T> {

	/**
     * Une pile représenté par une structure LinkedList pour le parcours en profondeur
     */
	private LinkedList<Node<T>> pile;
	
	public ExplorationProfondeur() {
		this.pile = new LinkedList<>();
	}
	
	@Override
	public Node<T> remove() {
		//dépiler l’élément en sommet de pile
		try {
			return this.pile.removeFirst();
		} catch(NoSuchElementException e) {
			return null;
		}
	}

	@Override
	public boolean add(Node<T> n) {
		//empiler un élément
		this.pile.addFirst(n);
		return true;
	}

	@Override
	public int size() {
		return this.pile.size();
	}

	@Override
	public Node<T> get() {
		return this.pile.getFirst();
	}

	/**
	 * @return the pile
	 */
	public LinkedList<Node<T>> getPile() {
		return pile;
	}

	/**
	 * @param pile the pile to set
	 */
	public void setPile(LinkedList<Node<T>> pile) {
		this.pile = pile;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pile == null) ? 0 : pile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		ExplorationProfondeur<T> other = (ExplorationProfondeur<T>) obj;
		if (pile == null) {
			if (other.pile != null)
				return false;
		} else if (!pile.equals(other.pile))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ExplorationProfondeur [pile=" + pile + "]";
	}

	@Override
	public String getNom() {
		return this.getClass().getSimpleName();
	}

	@Override
	public void clear() {
		this.pile.clear();
	}
}
