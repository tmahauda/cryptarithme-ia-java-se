package ia.search.explorable;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import ia.search.Node;
import ia.solve.Solveable;

/**
 * Exploration avec une file
 * Une file est une liste d’éléments dans laquelle tout insertion se fait à une
 * extrémité de la file, appelé queue, et toute suppression se fait à une
 * extrémité de la file, appelée tête
 * 
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class ExplorationLargeur<T extends Solveable> implements Explorable<T> {

	/**
     * Une file représenté par une structure LinkedList pour le parcours en largeur
     */
	private LinkedList<Node<T>> file;
	
	public ExplorationLargeur() {
		this.file = new LinkedList<>();
	}
	
	@Override
	public Node<T> remove() {
		//supprimer l’élément en tête de file
		try {
			return this.file.remove();
		} catch(NoSuchElementException e) {
			return null;
		}
	}

	@Override
	public boolean add(Node<T> n) {
		//ajouter un élément en queue de file
		return this.file.add(n);
	}

	@Override
	public int size() {
		return this.file.size();
	}

	@Override
	public Node<T> get() {
		return this.file.peek();
	}

	/**
	 * @return the file
	 */
	public LinkedList<Node<T>> getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(LinkedList<Node<T>> file) {
		this.file = file;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((file == null) ? 0 : file.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		ExplorationLargeur<T> other = (ExplorationLargeur<T>) obj;
		if (file == null) {
			if (other.file != null)
				return false;
		} else if (!file.equals(other.file))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ExplorationLargeur [file=" + file + "]";
	}

	@Override
	public String getNom() {
		return this.getClass().getSimpleName();
	}

	@Override
	public void clear() {
		this.file.clear();
	}
}
