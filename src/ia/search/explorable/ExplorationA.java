package ia.search.explorable;

import java.util.NoSuchElementException;
import java.util.PriorityQueue;
import ia.search.Node;
import ia.solve.Solveable;

/**
 * Exploration avec une file prioritaire
 * Une file de priorité est une file dans laquelle l’ordre des éléments ne dépend
 * plus de leur date d’insertion mais d’une priorité entre eux
 * Pour définir la relation d’ordre entre les éléments, la classe Node implémente l’interface Comparable<E>, et en
 * particulier la méthode int compareTo(Node node)
 * 
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class ExplorationA<T extends Solveable> implements Explorable<T> {

	/**
     * Une file prioritère par une structure PriorityQueue pour le parcours en A*
     */
	private PriorityQueue<Node<T>> filePriority;
	
	public ExplorationA() {
		this.filePriority = new PriorityQueue<>();
	}
	
	@Override
	public Node<T> remove() {
		//supprimer l’élément en tête de file
		try {
			return this.filePriority.remove();
		} catch(NoSuchElementException e) {
			return null;
		}
	}

	@Override
	public boolean add(Node<T> n) {
		//ajouter un élément en queue de file
		return this.filePriority.add(n);
	}

	@Override
	public int size() {
		return this.filePriority.size();
	}

	@Override
	public Node<T> get() {
		return this.filePriority.peek();
	}

	/**
	 * @return the filePriority
	 */
	public PriorityQueue<Node<T>> getFilePriority() {
		return filePriority;
	}

	/**
	 * @param filePriority the filePriority to set
	 */
	public void setFilePriority(PriorityQueue<Node<T>> filePriority) {
		this.filePriority = filePriority;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filePriority == null) ? 0 : filePriority.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		ExplorationA<T> other = (ExplorationA<T>) obj;
		if (filePriority == null) {
			if (other.filePriority != null)
				return false;
		} else if (!filePriority.equals(other.filePriority))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ExplorationA [filePriority=" + filePriority + "]";
	}

	@Override
	public String getNom() {
		return this.getClass().getSimpleName();
	}

	@Override
	public void clear() {
		this.filePriority.clear();
	}
}
