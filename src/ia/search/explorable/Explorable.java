package ia.search.explorable;

import ia.search.Node;
import ia.solve.Solveable;

 /** 
 * La classe Explorable, appelé Frontière, représente la collection des noeuds générés mais non encore explorés utilisée par l'algorithme de recherche
 *
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public interface Explorable<T extends Solveable> {

	/**
     * Méthode qui permet de supprimer et de récupérer le noeud en tête de la structure
     * @return noeud en tête
     */
	public Node<T> remove();
	
	/**
     * Méthode qui permet d'ajouter un noeud en tête de la structure
     * @param n le noeud à ajouter
     */
	public boolean add(Node<T> n);
	
	/**
     * Méthode qui permet de récupérer la taille de la structure
     * @return la taille
     */
	public int size();
	
	/**
     * Méthode qui permet de récupérer le noeud en tête de structure sans pour autant la supprimer
     * @return le noeud en tête
     */
	public Node<T> get();
	
	/**
	 * Nettoyer la frontiere
	 */
	public void clear();
	
	/**
	 * Récupérer le nom de l'exploration
	 */
	public String getNom();
}
