package ia.search;

import java.util.ArrayList;
import java.util.List;
import ia.search.explorable.Explorable;
import ia.solve.Problem;
import ia.solve.Solveable;
import ia.solve.heuristic.Heuristic;

 /**
 * La classe Search permet d'explorer en arbre le probleme pour trouver la solution selon une stratégie
 * - Parcours en profondeur qui implémente une structure LIFO = pile
 * - Parcours en largueur qui implémente une structure FIFO = file
 * - Parcours en A* qui implémente une structure FIFO PRIORITY = file prioritaire en s'aidant d'une fonction heuristique
 * 
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class Search<T extends Solveable> {
	
	/**
	 * Le problème à résoudre
	 */
	private Problem<T> problem;
	
	/**
	 * Le mode d'exploration de l'arbre
	 */
	private Explorable<T> frontier;
	
	/**
	 * L'heuristique choisi pour le parcours en A*
	 */
	private Heuristic<T> heuristic;
	
	/**
	 * Liste fermé qui mémorise tous les noeuds développés
	 */
	private List<Node<T>> closeList;

	public Search(Problem<T> problem, Explorable<T> frontier, Heuristic<T> heuristic) {
		this.problem = problem;
		this.frontier = frontier;
		this.heuristic = heuristic;
		this.closeList = new ArrayList<>();
	}
	
	/**
	 * Permet d'explorer en arbre selon une stratégie de résolution
	 * @return le noeud qui permet de résoudre le probleme
	 */
	public Node<T> explore() {
		
		Node.setTotalNodeCreate(0);
		boolean developLeaf = false;
		boolean findSolution = false;
		int limite = 0;
		Node<T> currentNode = null;
		
		try {
			@SuppressWarnings("unchecked")
			Node<T> root = new Node<T>((T)this.problem.getStateInitial().clone(), null, "aucun", 0, 0, this.heuristic);
			this.frontier.add(root);	
		} 
		catch(CloneNotSupportedException e) {
			return null;
		}

		//Tant que la solution n'est pas trouvé selon une certaine limite
		while((!findSolution) && limite < Integer.MAX_VALUE) {
			
			//On récupère le noeud dans la strucuture de donnée
			//pile, file ou file_prioritaire selon la stratégie optée
			currentNode = this.frontier.remove();
			
			if(currentNode == null) return null;
			
			//On vérifie si la solution a été trouvé
			findSolution = this.problem.checkSoluce(currentNode.getEtat());
			
			//Si la solution a été trouvé dans ce cas on retourne le noeud courant
			if(findSolution) return currentNode;
			
			//Si le noeud courant n'a pas déjà été recontré dans le développement
			developLeaf = this.checkDevelopLeaf(currentNode);
					
			//Si oui dans ce cas on développe les feuilles du noeud courant
			if(developLeaf) {
				this.developLeaf(currentNode);
				//On a exploré ce noeud donc nul besoin de le redévelopper ultérieurement
				//si on recroise cette situation. On l'ajoute dans la liste fermé
				this.closeList.add(currentNode);
			}
			
			limite++;
		}
		
		return currentNode;
	}
	
	/**
     * Méthode qui permet de vérifier le noeud courant est déjà
     * dans la liste fermée ou non, pour éviter de le redévelopper
     * @param listeFerme qui mémorise tous les noeuds développés
     * @param noeud qui permet de vérifier s'il est déjà présent ou pas
     * @return true ou false
     */
	private boolean checkDevelopLeaf(Node<T> node) {
		for(Node<T> closeNode : this.closeList) {
			if(closeNode.equals(node)) {
				return false;
			}
		}
		return true;
	}
	
	/**
     * Méthode qui permet de développer les feuilles grâce au(x) opérateur(s) du noeud courant
     * selon la stratégie mis en place et de les ajouter dans la frontiere
     * @param noeud dont on veut développer ses feuilles
     * @param strategie pour connaitre l'ordre de création des noeuds feuilles
     * @param frontiere pour ajouter les feuilles crées
     */
	private void developLeaf(Node<T> node) {
		//Pour chaque action
		for(int action : node.getEtat().getActions()) {

			//Qui sont vérifiés et réalisable
			if(node.getEtat().checkActions(action)) {
				
				try {
					//On fait une copie profonde de l'état courant
					@SuppressWarnings("unchecked")
					T newEtat = (T)node.getEtat().clone();
					
					//On applique l'action sur le noeud avec les toutes les règles
					String act = newEtat.applyActions(action);
					
					//Puis on ajoute cette feuille dans l'exploration
					Node<T> leaf = new Node<>(newEtat, node, act, node.getCout()+1, node.getProfondeur()+1, this.heuristic);
					this.frontier.add(leaf);
				} 
				catch(CloneNotSupportedException e) {
					return;
				}
			}
		}
		
		//On effectue un nettoyage des actions générées afin d'éviter un java.lang.OutOfMemoryError
		node.getEtat().clear();
	}
}
