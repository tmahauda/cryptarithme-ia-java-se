package ia.solve.heuristic;

import ia.search.Node;
import ia.solve.Solveable;

/**
 * Fonction heuristique, h(n), qui permet de calculer le coût estimé du chemin le
 * moins coûteux du noeud n à un noeud but.
 *
 * @param <T> le problème à résoudre
 * 
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public abstract class Heuristic<T extends Solveable> {

	protected double h;
	protected double g;
	protected double f;
	
	public Heuristic() {
		
	}
	
	/**
	 * Récupérer le nom de l'heuristique
	 */
	public String getName() {
		return this.getClass().getSimpleName();
	}
	
	/**
     * Permet de calculer la fonction heuristique h(n) 
     * @return le cout estimé du meilleur chemin du noeud n au noeud but
     */
	public abstract double calculHHeuristic(Node<T> n);
	
	/**
     * Permet de calculer la fonction heuristique g(n)
     * @return le cout exact du chemin entre le noeud n et le noeud but
     */
	public abstract double calculGHeuristic(Node<T> n);
	
	/**
     * Permet de calculer la fonction heuristique f(n) 
     * @return le cout estimé du meilleur chemin passant par n
     */
	public double calculFHeuristic(Node<T> n) {
		this.f = this.calculHHeuristic(n) + this.calculGHeuristic(n);
		return this.f;
	}

	/**
	 * @return the h
	 */
	public double getH() {
		return h;
	}

	/**
	 * @param h the h to set
	 */
	public void setH(double h) {
		this.h = h;
	}

	/**
	 * @return the g
	 */
	public double getG() {
		return g;
	}

	/**
	 * @param g the g to set
	 */
	public void setG(double g) {
		this.g = g;
	}

	/**
	 * @return the f
	 */
	public double getF() {
		return f;
	}

	/**
	 * @param f the f to set
	 */
	public void setF(double f) {
		this.f = f;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(f);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(g);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(h);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		Heuristic<T> other = (Heuristic<T>) obj;
		if (Double.doubleToLongBits(f) != Double.doubleToLongBits(other.f))
			return false;
		if (Double.doubleToLongBits(g) != Double.doubleToLongBits(other.g))
			return false;
		if (Double.doubleToLongBits(h) != Double.doubleToLongBits(other.h))
			return false;
		return true;
	}
}
