package ia.solve.heuristic;

import cryptarithme.CryptarithmeAdapter;
import cryptarithme.constraint.Constraint;
import ia.search.Node;

/**
 * Fonction heuristique, h(n), qui permet de calculer le coût estimé du chemin le
 * moins coûteux du noeud n à un noeud but.
 *
 * @param <T> le problème à résoudre
 * 
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class HeuristicCryptarithme extends Heuristic<CryptarithmeAdapter> {

	/**
	 * Deux heuristiques à calculer :
	 * - La différence entre les variables et les conditions dont il faut qu'il soit proche de 0
	 * - La différence entre le nombre de variables totales et le nombre de variables différents dont il faut qu'il soit proche de 0
	 */
	@Override
	public double calculHHeuristic(Node<CryptarithmeAdapter> n) {
		double h = 0;
		
		for(Constraint constraint : n.getEtat().getCryptarithme().getConstraints()) {
			h += constraint.calculHHeuristic();
		}
		
		this.h = h;
		return this.h;
	}

	/**
	 * Nombre de mouvements ayant mené à l’état courant
	 */
	@Override
	public double calculGHeuristic(Node<CryptarithmeAdapter> n) {
		this.g = n.getCout();
		return this.g;
	}

}
