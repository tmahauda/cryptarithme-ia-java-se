package ia.solve;

import java.util.List;

 /**
 * La classe Probleme représente un problème à résoudre à partir d'une situation initial
 * pour obtenir un des états finaux
 * 
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class Problem<T extends Solveable> {
	
	/**
     * Un état initial
     * @see Situation
     */
	protected T stateInitial;
	
	/**
     * Un ensemble d'états finaux (ou buts) ou bien un test d'état final qui détermine si un état donné est ou non un état final
     * @see Situation
     */
	protected List<T> statesFinals;
	
	
	public Problem(T stateInitial, List<T> statesFinals) {
		this.stateInitial = stateInitial;
		this.statesFinals = statesFinals;
	}

	/**
	 * Vérifier si un état est présent dans les états finaux
	 * @param state
	 * @return
	 */
	public boolean checkSoluce(T state) {
		for(T s : this.statesFinals) {
			if(s.equals(state))
				return true;
		}
		return false;
	}

	/**
	 * @return the stateInitial
	 */
	public T getStateInitial() {
		return stateInitial;
	}

	/**
	 * @param stateInitial the stateInitial to set
	 */
	public void setStateInitial(T stateInitial) {
		this.stateInitial = stateInitial;
	}

	/**
	 * @return the stateFinals
	 */
	public List<T> getStateFinal() {
		return statesFinals;
	}

	/**
	 * @param stateFinals the stateFinals to set
	 */
	public void setStateFinal(List<T> statesFinals) {
		this.statesFinals = statesFinals;
	}

	/**
	 * @return the statesFinals
	 */
	public List<T> getStatesFinals() {
		return statesFinals;
	}

	/**
	 * @param statesFinals the statesFinals to set
	 */
	public void setStatesFinals(List<T> statesFinals) {
		this.statesFinals = statesFinals;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((stateInitial == null) ? 0 : stateInitial.hashCode());
		result = prime * result + ((statesFinals == null) ? 0 : statesFinals.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		Problem<T> other = (Problem<T>) obj;
		if (stateInitial == null) {
			if (other.stateInitial != null)
				return false;
		} else if (!stateInitial.equals(other.stateInitial))
			return false;
		if (statesFinals == null) {
			if (other.statesFinals != null)
				return false;
		} else if (!statesFinals.equals(other.statesFinals))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Problem [stateInitial=" + stateInitial + ", statesFinals=" + statesFinals + "]";
	}
}

