package ia.solve;

import java.util.Collections;
import cryptarithme.CryptarithmeAdapter;

 /**
 * La classe Probleme représente un problème à résoudre à partir d'une situation initial
 * 
 * Created on : 10/01/2020
 * Copyright : Master in computer science at the university of angers
 * @author Théo MAHAUDA, Anas TAGUENITI
 * @version 1.0
 */
public class ProblemCryptarithme extends Problem<CryptarithmeAdapter> {

	public ProblemCryptarithme(CryptarithmeAdapter stateInitial) {
		super(stateInitial, Collections.emptyList());
	}
	
	@Override
	public boolean checkSoluce(CryptarithmeAdapter state) {
		return state.soluceFind();
	}
}

