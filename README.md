# Cryptarithme

<div align="center">
<img width="500" height="400" src="cryptarithme.png">
</div>

## Description du projet

Application console réalisée avec Java SE en MASTER INFO 1 à l'université d'Angers dans le cadre du module "Intelligence Artificielle 1" durant l'année 2019-2020 avec un groupe de deux personnes.
Un cryptarithme ou jeu cryptarithmétique est un casse-tête numérique et logique qui consiste en une équation mathématique où les lettres doivent être remplacées par des chiffres, 
deux lettres ne pouvant pas avoir la même valeur. L'un des plus connu car probablement le plus ancien est le problème SEND+MORE=MONEY.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par :
- Théo MAHAUDA : tmahauda@etud.univ-angers.fr ;

### Encadrants

Ce projet fut encadré par un enseignant de l'université d'Angers :
- Jean-Michel RICHER : jean-michel.richer@univ-angers.fr.

## Organisation

Ce projet a été agit au sein de l'université d'Angers dans le cadre du module "Intelligence Artificielle" du MASTER INFO 1.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2019 sur la période du module pendant les heures de TP et personnelles à la maison. \
Il a été terminé et rendu le 10/01/2020.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse ;
- Algorithmes A* et AO*.

## Objectifs

Pour résoudre ce genre de problème il est possible de le formuler comme une CSP (Constraint Satisfaction Problem) pour lequel on doit définir :
- des domaines de valeurs : D={d1,...,dm}
- des variables : V={v1,...,vn}
- des contraintes C={c1,...,ck} exprimées entre les variables

Pour SEND+MORE=MONEY, on a donc :
- D={d1,d2} avec d1={0,1} et d2={0,...,9}
- V={S,E,N,D,M,O,R,Y,R1,R2,R3,R4}
  - les variables {S,E,N,D,M,O,R,Y} prennent leurs valeurs dans d2
  - les retenues {R1,R2,R3,R4} prennent leurs valeurs dans d1
- des contraintes C={c1,c2,c3,c4} sont les suivantes :
  - c1 (addition) : D+E=Y+10×R1
  - c2 (addition) : R1+N+R=E+10×R2
  - c3 (addition) : R2+E+O=N+10×R3
  - c4 (addition) : R3+S+M=O+10×R4
  - c5 (égalité) : R4=M
  - c6 (alldiff) : S,E,N,D,M,O,R,Y

Plus de détail sur :
- http://info.univ-angers.fr/~richer/ensm1i_ia_jeux.php
- https://fr.wikipedia.org/wiki/Cryptarithme

Pour éxécuter le programme, il faut faire par exemple

java cryptarithme.Main ZERO NEUF NEUF DOUZE TRENTE

Si aucun argument n'est fourni, le programme résout par défaut le problème SEND + MORE = MONEY

Pour cette exemple le programme affiche le résultat suivant : 

Résolution de ZERO + NEUF + NEUF + DOUZE = TRENTE en cours. Veuillez patienter
Une solution est : 
Z = 9 € [1, 2, 3, 4, 5, 6, 7, 8, 9]
E = 2 € [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
R = 0 € [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
O = 6 € [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
N = 3 € [1, 2, 3, 4, 5, 6, 7, 8, 9]
U = 5 € [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
F = 7 € [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
D = 8 € [1, 2, 3, 4, 5, 6, 7, 8, 9]
T = 1 € [1, 2, 3, 4, 5, 6, 7, 8, 9]